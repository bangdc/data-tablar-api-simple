var faker = require('faker');
var _ = require('lodash');
var fs = require('fs');
var jsonServer = require('json-server')
var bodyParser = require('body-parser');
var server = jsonServer.create()
var router = jsonServer.router('db.json')
var middlewares = jsonServer.defaults();

server.set('port', (process.env.PORT || 5003));

//USE MIDDLEWARES
//bodyParser for parsing request's payload.
server.use(bodyParser.json()); // for parsing application/json
server.use(bodyParser.urlencoded({
	extended: true
})); // for parsing application/x-www-form-urlencoded

//USE MIDDLEWARES of jsonServer
server.use(middlewares);

var NUMBER_OF_USERS = 500;
var NUMBER_OF_DEPARTMENTS = 6;
var NUMBER_OF_COLORS = 5;

var genders = ['male', 'female', 'other'];

var departments = [{
	"id": 0,
	"name": "Books"
}, {
	"id": 1,
	"name": "Baby"
}, {
	"id": 2,
	"name": "Grocery"
}, {
	"id": 3,
	"name": "Beauty"
}, {
	"id": 4,
	"name": "Computers"
}, {
	"id": 5,
	"name": "Industrial"
}];

var colors = [{
	"id": 0,
	"color": "orange"
}, {
	"id": 1,
	"color": "black"
}, {
	"id": 2,
	"color": "sky blue"
}, {
	"id": 3,
	"color": "cyan"
}, {
	"id": 4,
	"color": "red"
}];

var db = {
	genders: genders,
	colors: colors,
	users: _.times(NUMBER_OF_USERS, function(i) {
		return {
			id: i,
			firstName: faker.name.firstName(),
			email: faker.internet.email(),
			userName: faker.internet.userName(),
			avatar: faker.internet.avatar(),
			address: faker.address.city(),
			birthday: faker.date.past(),
			salary: faker.random.number() + 0.211916,
			isVip: faker.random.boolean(),
			shortDesc: 'ABOUT ME: ' + faker.lorem.paragraph(),
			website: 'http://datatablar.com',
			gender: genders[faker.random.number() % 2],
			favoriteColors: [colors[i % NUMBER_OF_COLORS].color],
			departmentId: i % NUMBER_OF_DEPARTMENTS,
			departmentName: departments[i % NUMBER_OF_DEPARTMENTS].name
		}
	}),
	departments: departments
}

fs.writeFileSync('db.json', JSON.stringify(db, null, 4), 'utf-8');

//ROUTING - Update a post - POST
server.patch('/users/:id', function(req, res, next) {
	var dbJSON = fs.readFileSync('db.json');
	dbJSON = JSON.parse(dbJSON);
	if (req.body.hasOwnProperty('departmentId')) {
		var departmentId = parseInt(req.body.departmentId);
		req.body.departmentName = dbJSON.departments[departmentId].name;
	}

	next();
});

server.put('/users/:id', function(req, res, next) {
	if (!req.body.hasOwnProperty('firstName')) {
		res.status(400).json({
			error: 'Missed parameter firstName'
		});
	} else if (!req.body.hasOwnProperty('email')) {
		res.status(400).json({
			error: 'Missed parameter email'
		});
	} else if (!req.body.hasOwnProperty('userName')) {
		res.status(400).json({
			error: 'Missed parameter userName'
		});
	} else if (!req.body.hasOwnProperty('avatar')) {
		res.status(400).json({
			error: 'Missed parameter avatar'
		});
	} else if (!req.body.hasOwnProperty('address')) {
		res.status(400).json({
			error: 'Missed parameter address'
		});
	} else if (!req.body.hasOwnProperty('birthday')) {
		res.status(400).json({
			error: 'Missed parameter birthday'
		});
	} else if (!req.body.hasOwnProperty('salary')) {
		res.status(400).json({
			error: 'Missed parameter salary'
		});
	} else if (!req.body.hasOwnProperty('isVip')) {
		res.status(400).json({
			error: 'Missed parameter isVip'
		});
	} else if (!req.body.hasOwnProperty('shortDesc')) {
		res.status(400).json({
			error: 'Missed parameter shortDesc'
		});
	} else if (!req.body.hasOwnProperty('website')) {
		res.status(400).json({
			error: 'Missed parameter website'
		});
	} else if (!req.body.hasOwnProperty('gender')) {
		res.status(400).json({
			error: 'Missed parameter gender'
		});
	} else if (!req.body.hasOwnProperty('favoriteColors')) {
		res.status(400).json({
			error: 'Missed parameter favoriteColors'
		});
	} else if (!req.body.hasOwnProperty('departmentId')) {
		res.status(400).json({
			error: 'Missed parameter departmentId'
		});
	} else {
		var dbJSON = fs.readFileSync('db.json');
		dbJSON = JSON.parse(dbJSON);

		var departmentId = parseInt(req.body.departmentId);
		req.body.departmentName = dbJSON.departments[departmentId].name;

		req.body.favoriteColors = JSON.parse(req.body.favoriteColors);
		next();
	}
});

server.post('/users', function(req, res, next) {
	if (!req.body.hasOwnProperty('firstName')) {
		res.status(400).json({
			error: 'Missed parameter firstName'
		});
	} else if (!req.body.hasOwnProperty('email')) {
		res.status(400).json({
			error: 'Missed parameter email'
		});
	} else if (!req.body.hasOwnProperty('userName')) {
		res.status(400).json({
			error: 'Missed parameter userName'
		});
	} else if (!req.body.hasOwnProperty('avatar')) {
		res.status(400).json({
			error: 'Missed parameter avatar'
		});
	} else if (!req.body.hasOwnProperty('address')) {
		res.status(400).json({
			error: 'Missed parameter address'
		});
	} else if (!req.body.hasOwnProperty('birthday')) {
		res.status(400).json({
			error: 'Missed parameter birthday'
		});
	} else if (!req.body.hasOwnProperty('salary')) {
		res.status(400).json({
			error: 'Missed parameter salary'
		});
	} else if (!req.body.hasOwnProperty('isVip')) {
		res.status(400).json({
			error: 'Missed parameter isVip'
		});
	} else if (!req.body.hasOwnProperty('shortDesc')) {
		res.status(400).json({
			error: 'Missed parameter shortDesc'
		});
	} else if (!req.body.hasOwnProperty('website')) {
		res.status(400).json({
			error: 'Missed parameter website'
		});
	} else if (!req.body.hasOwnProperty('gender')) {
		res.status(400).json({
			error: 'Missed parameter gender'
		});
	} else if (!req.body.hasOwnProperty('favoriteColors')) {
		res.status(400).json({
			error: 'Missed parameter favoriteColors'
		});
	} else if (!req.body.hasOwnProperty('departmentId')) {
		res.status(400).json({
			error: 'Missed parameter departmentId'
		});
	} else {
		var dbJSON = fs.readFileSync('db.json');
		dbJSON = JSON.parse(dbJSON);

		var departmentId = parseInt(req.body.departmentId);
		req.body.departmentName = dbJSON.departments[departmentId].name;

		req.body.favoriteColors = JSON.parse(req.body.favoriteColors);
		next();
	}
});

server.use(router);

server.listen(server.get('port'), function() {
	console.log('JSON Server is running')
})